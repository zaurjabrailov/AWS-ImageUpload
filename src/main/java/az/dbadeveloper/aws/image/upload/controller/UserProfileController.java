package az.dbadeveloper.aws.image.upload.controller;

import az.dbadeveloper.aws.image.upload.profile.UserProfile;
import az.dbadeveloper.aws.image.upload.service.UserProfileService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/v1/user-profile")
@RequiredArgsConstructor
@CrossOrigin("*") // In PROD not recommended give all access with *
public class UserProfileController {

    private final UserProfileService userProfileService;

    @GetMapping
    public ResponseEntity<List<UserProfile>> getUserProfile() {
        return ResponseEntity.ok().body(userProfileService.getUserProfile());
    }

    @PostMapping(
            path = "{userProfileId}/image/download",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserProfile> uploadUserProfileImage(@PathVariable UUID userProfileId,
                                                              @RequestParam MultipartFile file) {
        return ResponseEntity.created(userProfileService.uploadUserProfileImage(userProfileId, file)).build();
    }
}
