package az.dbadeveloper.aws.image.upload.service.impl;

import az.dbadeveloper.aws.image.upload.profile.UserProfile;

import az.dbadeveloper.aws.image.upload.repository.UserProfileDataAccessRepository;
import az.dbadeveloper.aws.image.upload.service.UserProfileService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserProfileServiceImpl implements UserProfileService {

    private final UserProfileDataAccessRepository userProfileDataAccessRepository;

    @Override
    public List<UserProfile> getUserProfile() {
        return userProfileDataAccessRepository.getUserProfile();
    }

    //1. Check id image is not empty
    //2. If file is an image
    //3. The user exists in our database
    //4. Grab some metadata from file if any
    //5 Store the image in S3 and update database (userProfileImageLink in field) with S3 image link
    @Override
    public URI uploadUserProfileImage(UUID userProfileId, MultipartFile file) {
        return null;
    }
}
