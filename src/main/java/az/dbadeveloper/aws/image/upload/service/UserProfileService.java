package az.dbadeveloper.aws.image.upload.service;

import az.dbadeveloper.aws.image.upload.profile.UserProfile;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.util.List;
import java.util.UUID;

public interface UserProfileService {

    List<UserProfile> getUserProfile();

    URI uploadUserProfileImage(UUID userProfileId, MultipartFile file);
}
