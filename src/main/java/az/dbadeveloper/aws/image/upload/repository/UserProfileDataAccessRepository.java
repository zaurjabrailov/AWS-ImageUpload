package az.dbadeveloper.aws.image.upload.repository;

import az.dbadeveloper.aws.image.upload.datastore.FakeUserProfileDataStore;
import az.dbadeveloper.aws.image.upload.profile.UserProfile;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
@RequiredArgsConstructor
public class UserProfileDataAccessRepository {

    private final FakeUserProfileDataStore fakeUserProfileDataStore;

    public List<UserProfile> getUserProfile() {
        return fakeUserProfileDataStore.getUserProfiles();
    }
}
